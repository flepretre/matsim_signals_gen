# signals_gen

Python generator for MATSim signals xml files

## setup

- use `venv` (https://github.com/juliendehos/venv)

## prerequisites

### softwares and data files

- osmosis (http://wiki.openstreetmap.org/wiki/Osmosis)
- OpenStreetMap data file `.pbf` (http://download.geofabrik.de/)
- MATSim network file `.xml` (test files are available in `input/` folder for *Calais* city)

### extracting data examples

Extract OpenStreetMap data with osmosis `.osm` (example for *Calais*):
```
cd osmosis/
./bin/osmosis --rbf nord-pas-de-calais-latest.osm.pbf --bounding-box top=50.9759 left=1.7965 bottom=50.9231 right=1.9222 --wx calais.osm
```

Extract traffic lights data `.xml`:
```
./bin/osmosis --rb file=calais.osm --tf accept-nodes highway=traffic_signals --wx signals.osm.xml
```

## install

    venv i signals_gen
    venv a signals_gen
    pip install lxml

## usage

    ./signals_gen.py
